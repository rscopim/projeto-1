resource "aws_instance" "web-prod" {
  ami           = "ami-0b8b44ec9a8f90422"
  instance_type = "t2.micro"
  
  tags = {
    Name = "VM-Teste"
  }
    vpc_security_group_ids = [aws_security_group.libera-acessos.id]
 
     key_name = "Terraform"
}